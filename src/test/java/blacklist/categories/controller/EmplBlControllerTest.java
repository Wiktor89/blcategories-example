package blacklist.categories.controller;

import blacklist.categories.dto.EmplBlDto;
import blacklist.categories.model.EmplBl;
import blacklist.categories.repository.EmplBlRepo;
import blacklist.categories.utils.GeneratorTestEntity;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.util.Random;

import static org.junit.Assert.assertNotNull;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.options;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
public class EmplBlControllerTest {

  private MockMvc mock;

  @Autowired
  private EmplBlRepo repo;

  @Autowired
  private WebApplicationContext webApplicationContext;

  @Autowired
  private ModelMapper converter;

  @Autowired
  private ObjectMapper objectMapper;

  @Before
  public void setUp() {
    mock = webAppContextSetup(webApplicationContext).build();
    Random random = new Random();
    random.ints().limit(20).forEach(value -> repo.save(GeneratorTestEntity.createEmplBl()));
  }

  @After
  public void tearDown() {
    repo.deleteAll();
  }

  @Test
  public void optionsAPI() throws Exception {
    this.mock.perform(options("/api/v1/blacklist/empl"))
        .andDo(print())
        .andExpect(status().isOk());
  }

  @Test
  public void findAllByIsActive() throws Exception {
    String content = this.mock.perform(get("/api/v1/blacklist/empl/")
        .param("signOfActivity", String.valueOf(Boolean.TRUE)))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(APPLICATION_JSON_UTF8))
        .andReturn().getResponse().getContentAsString();
    assertNotNull("Content empty", content);
  }

  @Test
  public void findAllByInActive() throws Exception {
    String content = this.mock.perform(get("/api/v1/blacklist/empl/")
        .param("signOfActivity", String.valueOf(Boolean.FALSE)))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(APPLICATION_JSON_UTF8))
        .andReturn().getResponse().getContentAsString();
    assertNotNull("Content empty", content);
  }

  @Test
  public void findByBin() throws Exception {
    EmplBl entity = repo.findAll().get(0);
    String content = this.mock.perform(get("/api/v1/blacklist/empl/filter")
        .param("bin", entity.getBin()))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(APPLICATION_JSON_UTF8))
        .andReturn().getResponse().getContentAsString();
    assertNotNull("Content empty", content);
    Assert.assertEquals("Objects are not equal", entity.getId(), tryParseMessage(content).getId());
  }

  @Test
  public void save() throws Exception {
    this.mock.perform(post("/api/v1/blacklist/empl/")
        .contentType(APPLICATION_JSON_UTF8).content(json(converter.map(GeneratorTestEntity.createEmplBl(), EmplBlDto.class))))
        .andDo(print()).andExpect(status().isCreated());
  }

  @Test
  public void deleteById() throws Exception {
    EmplBl entity = repo.findAll().get(0);
    this.mock.perform(delete("/api/v1/blacklist/empl/".concat(String.valueOf(entity.getId()))))
        .andDo(print())
        .andExpect(status().isOk());
  }

  @Test
  public void update() throws Exception {
    EmplBl entity = repo.findAll().get(0);
    this.mock.perform(put("/api/v1/blacklist/empl/".concat(String.valueOf(entity.getId())))
        .contentType(APPLICATION_JSON_UTF8)
        .content(json(converter.map(GeneratorTestEntity.createEmplBl(), EmplBlDto.class))))
        .andDo(print())
        .andExpect(status().isOk());
  }

  private String json(Object o) throws IOException {
    return objectMapper.writeValueAsString(o);
  }

  private EmplBlDto tryParseMessage(String o) throws IOException {
    return objectMapper.readValue(o, EmplBlDto.class);
  }
}