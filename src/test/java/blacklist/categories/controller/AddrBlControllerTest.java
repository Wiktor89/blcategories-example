package blacklist.categories.controller;

import blacklist.categories.dto.AddrBlDto;
import blacklist.categories.model.AddrBl;
import blacklist.categories.repository.AddrBlRepo;
import blacklist.categories.utils.GeneratorTestEntity;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.util.Random;

import static org.junit.Assert.assertNotNull;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.options;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
public class AddrBlControllerTest {

  private MockMvc mock;

  @Autowired
  private AddrBlRepo repo;

  @Autowired
  private WebApplicationContext webApplicationContext;

  @Autowired
  private ModelMapper converter;

  @Autowired
  private ObjectMapper objectMapper;

  @Before
  public void setUp() {
    mock = webAppContextSetup(webApplicationContext).build();
    Random random = new Random();
    random.ints().limit(20).forEach(value -> repo.save(GeneratorTestEntity.createAddBl()));
  }

  @After
  public void tearDown() {
    repo.deleteAll();
  }

  @Test
  public void optionsAPI() throws Exception {
    this.mock.perform(options("/api/v1/blacklist/addr"))
        .andDo(print())
        .andExpect(status().isOk());
  }

  @Test
  public void findAllByIsActive() throws Exception {
    String content = this.mock.perform(get("/api/v1/blacklist/addr/")
        .param("signOfActivity", String.valueOf(Boolean.TRUE)))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(APPLICATION_JSON_UTF8))
        .andReturn().getResponse().getContentAsString();
    assertNotNull("Content empty", content);
  }

  @Test
  public void findAllByInActive() throws Exception {
    String content = this.mock.perform(get("/api/v1/blacklist/addr/")
        .param("signOfActivity", String.valueOf(Boolean.FALSE)))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(APPLICATION_JSON_UTF8))
        .andReturn().getResponse().getContentAsString();
    assertNotNull("Content empty", content);
  }

  @Test
  public void findByTownAndStreetAndHouseAndFlate() throws Exception {
    AddrBl entity = repo.findAll().get(0);
    String content = this.mock.perform(get("/api/v1/blacklist/addr/filter")
        .param("town", entity.getTown())
        .param("street", entity.getStreet())
        .param("house", entity.getHouse())
        .param("flate", entity.getFlate()))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(APPLICATION_JSON_UTF8))
        .andReturn().getResponse().getContentAsString();
    assertNotNull("Content empty", content);
    Assert.assertEquals("Objects are not equal", entity.getId(), tryParseMessage(content).getId());
  }

  @Test
  public void save() throws Exception {
    this.mock.perform(post("/api/v1/blacklist/addr/")
        .contentType(APPLICATION_JSON_UTF8).content(json(converter.map(GeneratorTestEntity.createAddBl(), AddrBlDto.class))))
        .andDo(print()).andExpect(status().isCreated());
  }

  @Test
  public void deleteById() throws Exception {
    AddrBl entity = repo.findAll().get(0);
    this.mock.perform(delete("/api/v1/blacklist/addr/".concat(String.valueOf(entity.getId()))))
        .andDo(print())
        .andExpect(status().isOk());
  }

  @Test
  public void update() throws Exception {
    AddrBl entity = repo.findAll().get(0);
    this.mock.perform(put("/api/v1/blacklist/addr/".concat(String.valueOf(entity.getId())))
        .contentType(APPLICATION_JSON_UTF8)
        .content(json(converter.map(GeneratorTestEntity.createAddBl(), AddrBlDto.class))))
        .andDo(print())
        .andExpect(status().isOk());
  }

  private String json(Object o) throws IOException {
    return objectMapper.writeValueAsString(o);
  }

  private AddrBlDto tryParseMessage(String o) throws IOException {
    return objectMapper.readValue(o, AddrBlDto.class);
  }
}