package blacklist.categories.mapper;

import blacklist.categories.dto.PhysBlDto;
import blacklist.categories.model.PhysBl;
import blacklist.categories.utils.GeneratorTestEntity;
import org.junit.Test;
import org.modelmapper.ModelMapper;

import static org.junit.Assert.assertEquals;

public class PhysBlDataConversionTest {

  private ModelMapper mapper = new ModelMapper();

  @Test
  public void convertPhysBlToPhysBlDto() {
    PhysBl entity = GeneratorTestEntity.createPhysBl();

    PhysBlDto physBlDto = mapper.map(entity, PhysBlDto.class);

    assertEquals(entity.getActivatedBy(), physBlDto.getActivatedBy());
    assertEquals(entity.getActivatedDate(), physBlDto.getActivatedDate());
    assertEquals(entity.getActComments(), physBlDto.getActComments());
    assertEquals(entity.getIsActive(), physBlDto.getIsActive());
    assertEquals(entity.getDeactivatedBy(), physBlDto.getDeactivatedBy());
    assertEquals(entity.getDeactivatedDate(), physBlDto.getDeactivatedDate());
    assertEquals(entity.getDeactivatedComments(), physBlDto.getDeactivatedComments());
    assertEquals(entity.getModifiedDate(), physBlDto.getModifiedDate());
    assertEquals(entity.getModifiedBy(), physBlDto.getModifiedBy());

    assertEquals(entity.getLName(), physBlDto.getLName());
    assertEquals(entity.getFName(), physBlDto.getFName());
    assertEquals(entity.getMName(), physBlDto.getMName());
    assertEquals(entity.getBDate(), physBlDto.getBDate());
    assertEquals(entity.getIin(), physBlDto.getIin());
  }
}
