package blacklist.categories.mapper;

import blacklist.categories.dto.AddrBlDto;
import blacklist.categories.model.AddrBl;
import blacklist.categories.utils.GeneratorTestEntity;
import org.junit.Test;
import org.modelmapper.ModelMapper;

import static org.junit.Assert.assertEquals;

public class AddrBlDataConversionTest {

  private ModelMapper mapper = new ModelMapper();

  @Test
  public void convertAddrBlToAddrBlDto() {
    AddrBl entity = GeneratorTestEntity.createAddBl();

    AddrBlDto addrBlDto = mapper.map(entity, AddrBlDto.class);

    assertEquals(entity.getActivatedBy(), addrBlDto.getActivatedBy());
    assertEquals(entity.getActivatedDate(), addrBlDto.getActivatedDate());
    assertEquals(entity.getActComments(), addrBlDto.getActComments());
    assertEquals(entity.getIsActive(), addrBlDto.getIsActive());
    assertEquals(entity.getDeactivatedBy(), addrBlDto.getDeactivatedBy());
    assertEquals(entity.getDeactivatedDate(), addrBlDto.getDeactivatedDate());
    assertEquals(entity.getDeactivatedComments(), addrBlDto.getDeactivatedComments());
    assertEquals(entity.getModifiedDate(), addrBlDto.getModifiedDate());
    assertEquals(entity.getModifiedBy(), addrBlDto.getModifiedBy());

    assertEquals(entity.getName(), addrBlDto.getName());
    assertEquals(entity.getTown(), addrBlDto.getTown());
    assertEquals(entity.getStreet(), addrBlDto.getStreet());
    assertEquals(entity.getHouse(), addrBlDto.getHouse());
    assertEquals(entity.getFlate(), addrBlDto.getFlate());
  }
}
