package blacklist.categories.mapper;

import blacklist.categories.dto.PhoneBlDto;
import blacklist.categories.model.PhoneBl;
import blacklist.categories.utils.GeneratorTestEntity;
import org.junit.Test;
import org.modelmapper.ModelMapper;

import static org.junit.Assert.assertEquals;

public class PhoneBlDataConversionTest {

  private ModelMapper mapper = new ModelMapper();

  @Test
  public void convertPhoneBlToPhoneBlDto() {
    PhoneBl entity = GeneratorTestEntity.createPhoneBl();

    PhoneBlDto phoneBlDto = mapper.map(entity, PhoneBlDto.class);

    assertEquals(entity.getActivatedBy(), phoneBlDto.getActivatedBy());
    assertEquals(entity.getActivatedDate(), phoneBlDto.getActivatedDate());
    assertEquals(entity.getActComments(), phoneBlDto.getActComments());
    assertEquals(entity.getIsActive(), phoneBlDto.getIsActive());
    assertEquals(entity.getDeactivatedBy(), phoneBlDto.getDeactivatedBy());
    assertEquals(entity.getDeactivatedDate(), phoneBlDto.getDeactivatedDate());
    assertEquals(entity.getDeactivatedComments(), phoneBlDto.getDeactivatedComments());
    assertEquals(entity.getModifiedDate(), phoneBlDto.getModifiedDate());
    assertEquals(entity.getModifiedBy(), phoneBlDto.getModifiedBy());

    assertEquals(entity.getName(), phoneBlDto.getName());
    assertEquals(entity.getPhoneNumber(), phoneBlDto.getPhoneNumber());
  }
}
