package blacklist.categories.mapper;

import blacklist.categories.dto.EmplBlDto;
import blacklist.categories.model.EmplBl;
import blacklist.categories.utils.GeneratorTestEntity;
import org.junit.Test;
import org.modelmapper.ModelMapper;

import static org.junit.Assert.assertEquals;

public class EmplBlDataConversionTest {

  private ModelMapper mapper = new ModelMapper();

  @Test
  public void convertEmplBlToEmplBlDto() {
    EmplBl entity = GeneratorTestEntity.createEmplBl();

    EmplBlDto emplBlDto = mapper.map(entity, EmplBlDto.class);

    assertEquals(entity.getActivatedBy(), emplBlDto.getActivatedBy());
    assertEquals(entity.getActivatedDate(), emplBlDto.getActivatedDate());
    assertEquals(entity.getActComments(), emplBlDto.getActComments());
    assertEquals(entity.getIsActive(), emplBlDto.getIsActive());
    assertEquals(entity.getDeactivatedBy(), emplBlDto.getDeactivatedBy());
    assertEquals(entity.getDeactivatedDate(), emplBlDto.getDeactivatedDate());
    assertEquals(entity.getDeactivatedComments(), emplBlDto.getDeactivatedComments());
    assertEquals(entity.getModifiedDate(), emplBlDto.getModifiedDate());
    assertEquals(entity.getModifiedBy(), emplBlDto.getModifiedBy());

    assertEquals(entity.getName(), emplBlDto.getName());
    assertEquals(entity.getBin(), emplBlDto.getBin());
  }
}
