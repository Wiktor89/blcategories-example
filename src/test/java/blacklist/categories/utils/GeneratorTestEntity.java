package blacklist.categories.utils;

import blacklist.categories.model.AddrBl;
import blacklist.categories.model.CategoriesCommonsFields;
import blacklist.categories.model.EmplBl;
import blacklist.categories.model.PhoneBl;
import blacklist.categories.model.PhysBl;
import net.bytebuddy.utility.RandomString;

import java.time.LocalDate;
import java.util.concurrent.ThreadLocalRandom;

public class GeneratorTestEntity {

  private static final int random = 20;

  public static AddrBl createAddBl() {
    AddrBl entity = new AddrBl();
    common(entity);
    entity.setName(RandomString.make(random));
    entity.setTown(RandomString.make(random));
    entity.setStreet(RandomString.make(random));
    entity.setHouse(RandomString.make(random));
    entity.setFlate(RandomString.make(random));
    return entity;
  }

  public static EmplBl createEmplBl() {
    EmplBl entity = new EmplBl();
    common(entity);
    entity.setName(RandomString.make(random));
    entity.setBin(genInt());
    return entity;
  }

  public static PhysBl createPhysBl() {
    PhysBl entity = new PhysBl();
    common(entity);
    entity.setLName(RandomString.make(random));
    entity.setFName(RandomString.make(random));
    entity.setMName(RandomString.make(random));
    entity.setMName(RandomString.make(random));
    entity.setBDate(LocalDate.now());
    entity.setIin(genInt());
    return entity;
  }

  public static PhoneBl createPhoneBl() {
    PhoneBl entity = new PhoneBl();
    common(entity);
    entity.setName(RandomString.make(random));
    entity.setPhoneNumber(genInt());
    return entity;
  }

  private static CategoriesCommonsFields common (CategoriesCommonsFields entity) {
    entity.setModifiedBy(RandomString.make(random));
    entity.setActComments(RandomString.make(random));
    entity.setIsActive(System.currentTimeMillis() % 2 == 0 ? Boolean.TRUE : Boolean.FALSE);
    entity.setDeactivatedComments(RandomString.make(random));
    return entity;
  }

  private static String genInt() {
    String s = String.valueOf(ThreadLocalRandom.current().nextLong());
    return s.substring(s.length() - 11);
  }
}
