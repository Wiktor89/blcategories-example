package blacklist.categories.utils;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.util.Assert;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.Charset;
import java.util.Base64;
import java.util.Collections;
import java.util.Objects;
import java.util.Optional;

public class RequestUtil {

  private RequestUtil() {
    throw new AssertionError("No RequestUtil instances for you!");
  }

  public static HttpServletRequest getCurrentRequest() {
    RequestAttributes attrs = RequestContextHolder.getRequestAttributes();
    Assert.state(attrs instanceof ServletRequestAttributes, "No current ServletRequestAttributes");
    return !(attrs instanceof ServletRequestAttributes) ? null : ((ServletRequestAttributes) attrs).getRequest();
  }

  public static String getBasicAuthHeader() {
    HttpServletRequest httpRequest = getCurrentRequest();
    return Objects.isNull(httpRequest) ? null : httpRequest.getHeader("Authorization");
  }

  public static Optional<User> getFromBasicAuth() {
    String authorization = getBasicAuthHeader();
    if (authorization != null && authorization.startsWith("Basic")) {
      String base64Credentials = authorization.substring("Basic".length()).trim();
      String credentials = new String(Base64.getDecoder().decode(base64Credentials), Charset.forName("UTF-8"));
      String[] values = credentials.split(":", 2);
      if (Objects.nonNull(values[0]) && Objects.nonNull(values[1])) {
        return Optional.of(new User(values[0], (new BCryptPasswordEncoder()).encode(values[1]), Collections.singleton(new SimpleGrantedAuthority("USER"))));
      }
    }
    return Optional.empty();
  }
}

