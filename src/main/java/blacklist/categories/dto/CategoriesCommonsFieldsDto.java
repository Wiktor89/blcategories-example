package blacklist.categories.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

@ApiModel(value = "Общие поля для справочников")
@Data
public abstract class CategoriesCommonsFieldsDto implements Serializable {

  @ApiModelProperty(notes = "Уникальный идентификатор id", required = true)
  private Long id;

  @ApiModelProperty(notes = "Инициатор заведения")
  private String activatedBy;

  @ApiModelProperty(notes = "Дата заведения")
  private LocalDateTime activatedDate;

  @ApiModelProperty(notes = "Комментарии по заведению")
  private String actComments;

  @ApiModelProperty(notes = "Признак активности записи")
  private Boolean isActive;

  @ApiModelProperty(notes = "Инициатор деактивации")
  private String deactivatedBy;

  @ApiModelProperty(notes = "Дата деактивации")
  private LocalDateTime deactivatedDate;

  @ApiModelProperty(notes = "Комментарии по деактивации")
  private String deactivatedComments;

  @ApiModelProperty(notes = "Дата изменения")
  private LocalDateTime modifiedDate = LocalDateTime.now();

  @ApiModelProperty(notes = "Кем изменено", required = true)
  @NotNull
  private String modifiedBy;
}
