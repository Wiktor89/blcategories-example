package blacklist.categories.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@ApiModel(value = "Справочник")
@Data
public class AddrBlDto extends CategoriesCommonsFieldsDto {

  @ApiModelProperty(notes = "Название")
  private String name;

  @ApiModelProperty(notes = "Город")
  @NotNull
  private String town;

  @ApiModelProperty(notes = "Улица")
  @NotNull
  private String street;

  @ApiModelProperty(notes = "Дом")
  @NotNull
  private String house;

  @ApiModelProperty(notes = "Квартира")
  private String flate;
}
