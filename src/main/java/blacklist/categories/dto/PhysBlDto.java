package blacklist.categories.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;

@ApiModel(value = "Справочник")
@Data
@EqualsAndHashCode(callSuper = false)
public class PhysBlDto extends CategoriesCommonsFieldsDto {

  @ApiModelProperty(notes = "Фамилия", required = true)
  @NotNull
  private String lName;

  @ApiModelProperty(notes = "Имя")
  private String fName;

  @ApiModelProperty(notes = "Отчество")
  private String mName;

  @ApiModelProperty(notes = "День рождения")
  @NotNull
  private LocalDate bDate;

  @ApiModelProperty(notes = "ИИН", required = true)
  @NotNull
  @Pattern(regexp = "[0-9]{11}", message = "номер не соответсвует regexp-у")
  private String iin;
}
