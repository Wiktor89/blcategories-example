package blacklist.categories.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

@ApiModel(value = "Справочник")
@Data
@EqualsAndHashCode(callSuper = false)
public class EmplBlDto extends CategoriesCommonsFieldsDto {

  @ApiModelProperty(notes = "Название")
  private String name;

  @ApiModelProperty(notes = "БИН", required = true)
  @NotNull
  private String bin;
}
