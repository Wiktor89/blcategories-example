package blacklist.categories.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.Pattern;

@ApiModel(value = "Справочник")
@Data
@EqualsAndHashCode(callSuper = false)
public class PhoneBlDto extends CategoriesCommonsFieldsDto {

  @ApiModelProperty(notes = "Название")
  private String name;

  @ApiModelProperty(notes = "Телефон")
  @Pattern(regexp = "[0-9]{11}", message = "Номер телефона не соответсвует regexp-у")
  private String phoneNumber;
}
