package blacklist.categories.repository;

import blacklist.categories.model.AddrBl;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AddrBlRepo extends JpaRepository<AddrBl, Long> {

  Optional<AddrBl> findByTownAndStreetAndHouseAndFlate(String town, String street, String house, String flate);

  Page<AddrBl> findAllByIsActive(Pageable pageable, Boolean aBoolean);
}
