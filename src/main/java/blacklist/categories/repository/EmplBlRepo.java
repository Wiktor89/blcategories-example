package blacklist.categories.repository;

import blacklist.categories.model.EmplBl;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EmplBlRepo extends JpaRepository<EmplBl, Long> {

  Optional<EmplBl> findByBin(String bin);

  Page<EmplBl> findAllByIsActive(Pageable pageable, Boolean aBoolean);
}
