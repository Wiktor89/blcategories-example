package blacklist.categories.repository;

import blacklist.categories.model.PhoneBl;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PhoneBlRepo extends JpaRepository<PhoneBl, Long> {

  Optional<PhoneBl> findByPhoneNumber(String phoneNumber);

  Page<PhoneBl> findAllByIsActive(Pageable pageable, Boolean aBoolean);
}
