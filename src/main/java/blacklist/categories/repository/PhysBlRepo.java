package blacklist.categories.repository;

import blacklist.categories.model.PhysBl;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Optional;

@Repository
public interface PhysBlRepo extends JpaRepository<PhysBl, Long> {

  Optional<PhysBl> findByLNameAndFNameAndMNameAndIinAndBDate(String lName, String fName,
                                                             String mName, String iin,
                                                             LocalDate bDate);

  Page<PhysBl> findAllByIsActive(Pageable pageable, Boolean aBoolean);
}
