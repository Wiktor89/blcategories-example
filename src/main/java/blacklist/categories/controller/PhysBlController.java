package blacklist.categories.controller;

import blacklist.categories.dto.PhysBlDto;
import blacklist.categories.exception.EntityNotFoundException;
import blacklist.categories.model.PhysBl;
import blacklist.categories.repository.PhysBlRepo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.time.LocalDate;
import java.util.Collection;

import static java.util.stream.Collectors.toList;

@Api(value = "PhysBlController: контроллер для работы с справочником")
@RestController
@RequestMapping("/api/v1/blacklist/phys")
@RequiredArgsConstructor
public class PhysBlController {

  private final PhysBlRepo physBlRepo;

  private final ModelMapper mapper;

  @RequestMapping(method = RequestMethod.OPTIONS)
  public ResponseEntity<?> optionsAPI() {
    return ResponseEntity.ok().allow(HttpMethod.GET, HttpMethod.POST, HttpMethod.DELETE, HttpMethod.PUT, HttpMethod.OPTIONS).build();
  }

  @ApiOperation(value = "Получить справочник", notes = "Получить справочник")
  @GetMapping(value = "/")
  public Collection<PhysBlDto> findAllBySingOfActive(@RequestParam(required = false, value = "page", defaultValue = "0") Integer page,
                                                     @RequestParam(required = false, value = "pageSize", defaultValue = "50") Integer pageSize,
                                                     @PathParam("signOfActivity") Boolean signOfActivity) {
    Page<PhysBl> result = physBlRepo.findAllByIsActive(pageSize == null ? Pageable.unpaged() : PageRequest.of(page, pageSize), signOfActivity);
    return result.stream()
        .map(val -> mapper.map(val, PhysBlDto.class))
        .collect(toList());
  }

  @ApiOperation(value = "Получить 'Черный список физических лиц'", notes = "Получить 'Черный список физических лиц'")
  @GetMapping(value = "/filter")
  public PhysBlDto findByLNameAndFNameAndMNameAndIinAndBDate(@PathParam("lName") String lName, @PathParam("fName") String fName, @PathParam("mName") String mName,
                                                             @PathParam("iin") String iin, @RequestParam("bDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate bDate) {
    PhysBl entity = physBlRepo.findByLNameAndFNameAndMNameAndIinAndBDate(lName, fName, mName, iin, bDate).orElseThrow(() -> new EntityNotFoundException("not found"));
    return mapper.map(entity, PhysBlDto.class);
  }

  @ApiOperation(value = "Добавить в справочник", notes = "Добавить в справочник")
  @ResponseStatus(HttpStatus.CREATED)
  @PostMapping(value = "/")
  public void save(@RequestBody PhysBlDto entity) {
    physBlRepo.save(mapper.map(entity, PhysBl.class));
  }

  @ApiOperation(value = "Удалить из справочника", notes = "Удалить из справочника")
  @ResponseStatus(value = HttpStatus.OK)
  @DeleteMapping(value = "/{id}")
  public void deleteById(@PathVariable("id") Long id) {
    PhysBl entity = physBlRepo.findById(id).orElseThrow(() -> new EntityNotFoundException("not found"));
    entity.setIsActive(Boolean.FALSE);
    physBlRepo.save(entity);
  }

  @ApiOperation(value = "Обновить в справочнике", notes = "Обновить в справочнике")
  @ResponseStatus(value = HttpStatus.OK)
  @PutMapping(value = "/{id}")
  public void update(@RequestBody PhysBlDto entity, @PathVariable("id") Long id) {
    PhysBl old = physBlRepo.findById(id).orElseThrow(() -> new EntityNotFoundException("not found"));
    entity.setId(old.getId());
    physBlRepo.save(mapper.map(entity, PhysBl.class));
  }
}
