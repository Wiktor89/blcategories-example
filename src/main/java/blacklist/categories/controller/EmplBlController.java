package blacklist.categories.controller;

import blacklist.categories.dto.EmplBlDto;
import blacklist.categories.exception.EntityNotFoundException;
import blacklist.categories.model.EmplBl;
import blacklist.categories.repository.EmplBlRepo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.util.Collection;

import static java.util.stream.Collectors.toList;

@Api(value = "EmplBlController: контроллер для работы с справочником")
@RestController
@RequestMapping("/api/v1/blacklist/empl")
@RequiredArgsConstructor
public class EmplBlController {

  private final EmplBlRepo emplBlRepo;

  private final ModelMapper mapper;

  @RequestMapping(method = RequestMethod.OPTIONS)
  public ResponseEntity<?> optionsAPI() {
    return ResponseEntity.ok().allow(HttpMethod.GET, HttpMethod.POST, HttpMethod.DELETE, HttpMethod.PUT, HttpMethod.OPTIONS).build();
  }

  @ApiOperation(value = "Получить справочник", notes = "Получить справочник")
  @GetMapping(value = "/")
  public Collection<EmplBlDto> findAllBySingOfActive(@RequestParam(required = false, value = "page", defaultValue = "0") Integer page,
                                                     @RequestParam(required = false, value = "pageSize", defaultValue = "50") Integer pageSize,
                                                     @PathParam("signOfActivity") Boolean signOfActivity) {
    Page<EmplBl> result = emplBlRepo.findAllByIsActive(pageSize == null ? Pageable.unpaged() : PageRequest.of(page, pageSize), signOfActivity);
    return result.stream()
        .map(val -> mapper.map(val, EmplBlDto.class))
        .collect(toList());
  }

  @ApiOperation(value = "Получить 'Черный список предприятий'", notes = "Получить 'Черный список предприятий'")
  @GetMapping(value = "/filter")
  public EmplBlDto findByBin(@PathParam("bin") String bin) {
    EmplBl entity = emplBlRepo.findByBin(bin).orElseThrow(() -> new EntityNotFoundException("not found"));
    return mapper.map(entity, EmplBlDto.class);
  }

  @ApiOperation(value = "Добавить в справочник", notes = "Добавить в справочник")
  @ResponseStatus(HttpStatus.CREATED)
  @PostMapping(value = "/")
  public void save(@RequestBody EmplBlDto entity) {
    emplBlRepo.save(mapper.map(entity, EmplBl.class));
  }

  @ApiOperation(value = "Удалить из справочника", notes = "Удалить из справочника")
  @ResponseStatus(value = HttpStatus.OK)
  @DeleteMapping(value = "/{id}")
  public void deleteById(@PathVariable("id") Long id) {
    EmplBl entity = emplBlRepo.findById(id).orElseThrow(() -> new EntityNotFoundException("not found"));
    entity.setIsActive(Boolean.FALSE);
    emplBlRepo.save(entity);
  }

  @ApiOperation(value = "Обновить в справочнике", notes = "Обновить в справочнике")
  @ResponseStatus(value = HttpStatus.OK)
  @PutMapping(value = "/{id}")
  public void update(@RequestBody EmplBlDto entity, @PathVariable("id") Long id) {
    EmplBl old = emplBlRepo.findById(id).orElseThrow(() -> new EntityNotFoundException("not found"));
    entity.setId(old.getId());
    emplBlRepo.save(mapper.map(entity, EmplBl.class));
  }
}
