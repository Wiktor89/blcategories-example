package blacklist.categories.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@ApiModel(value = "Справочник")
@Data
@Entity
@Table(name = "EMPL_BLACK_LIST_DICT",
    uniqueConstraints = {
        @UniqueConstraint(name = "bl_bin", columnNames = "BIN")},
    indexes = {
        @Index(name = "idx_comp_bl_1", columnList = "IS_ACTIVE"),
        @Index(name = "idx_comp_bl_5", columnList = "ACTIVATED_BY"),
        @Index(name = "idx_comp_bl_6", columnList = "DEACTIVATED_BY")})
@EqualsAndHashCode(callSuper = false)
public class EmplBl extends CategoriesCommonsFields {

  @ApiModelProperty(notes = "Название")
  @Column(name = "NAME")
  private String name;

  @ApiModelProperty(notes = "БИН", required = true)
  @Column(name = "BIN", nullable = false, columnDefinition = "VARCHAR(255) CHECK (BIN::text ~ '[0-9]{11}'::text)")
  @Pattern(regexp = "[0-9]{11}", message = "БИН номер не соответсвует regexp-у")
  @NotNull
  private String bin;
}
