package blacklist.categories.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Pattern;

@ApiModel(value = "Справочник")
@Data
@Entity
@Table(name = "PHONE_BLACK_LIST_DICT",
    uniqueConstraints = {
        @UniqueConstraint(name = "un_phones_bl_1", columnNames = {"PHONE_NUMBER"})},
    indexes = {
        @Index(name = "idx_phones_bl_1", columnList = "IS_ACTIVE"),
        @Index(name = "idx_phones_bl_5", columnList = "ACTIVATED_BY"),
        @Index(name = "idx_phones_bl_6", columnList = "DEACTIVATED_BY"),
        @Index(name = "idx_phones_bl_8", columnList = "PHONE_NUMBER"),
        @Index(name = "idx_phones_bl_7", columnList = "ID")})
@EqualsAndHashCode(callSuper = false)
public class PhoneBl extends CategoriesCommonsFields {

  @ApiModelProperty(notes = "Название")
  @Column(name = "NAME")
  private String name;

  @ApiModelProperty(notes = "Телефон")
  @Column(name = "PHONE_NUMBER", columnDefinition = "VARCHAR(255) CHECK (PHONE_NUMBER::text ~ '[0-9]{11}'::text)")
  @Pattern(regexp = "[0-9]{11}", message = "Номер телефона не соответсвует regexp-у")
  private String phoneNumber;
}
