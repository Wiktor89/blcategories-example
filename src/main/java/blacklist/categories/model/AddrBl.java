package blacklist.categories.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

@ApiModel(value = "Справочник")
@Data
@Entity
@Table(name = "ADDR_BLACK_LIST_DICT",
    uniqueConstraints = {
        @UniqueConstraint(name = "UN_ADDR_BLACKLIST", columnNames = {"TOWN", "STREET", "FLATE", "HOUSE"})},
    indexes = {
        @Index(name = "idx_addr_bl_1", columnList = "IS_ACTIVE"),
        @Index(name = "idx_addr_bl_5", columnList = "ACTIVATED_BY"),
        @Index(name = "idx_addr_bl_6", columnList = "DEACTIVATED_BY"),
        @Index(name = "idx_addr_bl_7", columnList = "ID", unique = true)})
@EqualsAndHashCode(callSuper = false)
public class AddrBl extends CategoriesCommonsFields {

  @ApiModelProperty(notes = "Название")
  @Column(name = "NAME")
  private String name;

  @ApiModelProperty(notes = "Город")
  @Column(name = "TOWN", nullable = false)
  @NotNull
  private String town;

  @ApiModelProperty(notes = "Улица")
  @Column(name = "STREET", nullable = false)
  @NotNull
  private String street;

  @ApiModelProperty(notes = "Дом")
  @Column(name = "HOUSE", nullable = false)
  @NotNull
  private String house;

  @ApiModelProperty(notes = "Квартира")
  @Column(name = "FLATE")
  private String flate;
}
