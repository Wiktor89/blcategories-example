package blacklist.categories.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;

@ApiModel(value = "Справочник")
@Data
@Entity
@Table(name = "PHYS_BLACK_LIST_DICT", uniqueConstraints = {
    @UniqueConstraint(name = "un_persons_bl_1", columnNames = {"IIN"}),
    @UniqueConstraint(name = "un_persons_bl_2", columnNames = {"lName", "fName", "mName", "iin"})},
    indexes = {
        @Index(name = "idx_pers_bl_1", columnList = "IS_ACTIVE"),
        @Index(name = "idx_pers_bl_5", columnList = "ACTIVATED_BY"),
        @Index(name = "idx_pers_bl_6", columnList = "DEACTIVATED_BY"),
        @Index(name = "idx_pers_bl_7", columnList = "IIN"),
        @Index(name = "idx_pers_bl_9", columnList = "lName,fName,mName,iin"),
        @Index(name = "idx_pers_bl_8", columnList = "ID")})
@EqualsAndHashCode(callSuper = false)
public class PhysBl extends CategoriesCommonsFields {

  @ApiModelProperty(notes = "Фамилия", required = true)
  @Column(name = "lName")
  @NotNull
  private String lName;

  @ApiModelProperty(notes = "Имя")
  @Column(name = "fName")
  private String fName;

  @ApiModelProperty(notes = "Отчество")
  @Column(name = "mName")
  private String mName;

  @ApiModelProperty(notes = "День рождения")
  @Column(name = "BDATE")
  @NotNull
  private LocalDate bDate;

  @ApiModelProperty(notes = "ИИН", required = true)
  @Column(name = "IIN", nullable = false, columnDefinition = "VARCHAR(255) CHECK (IIN::text ~ '[0-9]{11}'::text)")
  @NotNull
  @Pattern(regexp = "[0-9]{11}", message = "ИИН номер не соответсвует regexp-у")
  private String iin;
}
