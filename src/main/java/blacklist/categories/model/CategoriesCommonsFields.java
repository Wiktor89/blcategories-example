package blacklist.categories.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

@ApiModel(value = "Общие поля для справочников")
@Entity
@Data
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@EntityListeners(AuditingEntityListener.class)
public abstract class CategoriesCommonsFields implements Serializable {

  @ApiModelProperty(notes = "Уникальный идентификатор ID", required = true)
  @Id
  @Column(name = "ID", unique = true, nullable = false)
  @GeneratedValue(generator = "CATEGORIES_COMMONS_FIELDS_BLACK_LIST_DICT_SEQ", strategy = GenerationType.SEQUENCE)
  @SequenceGenerator(name = "CATEGORIES_COMMONS_FIELDS_BLACK_LIST_DICT_SEQ", sequenceName = "CATEGORIES_COMMONS_FIELDS_BLACK_LIST_DICT_SEQ", allocationSize = 1)
  @EqualsAndHashCode.Exclude
  private Long id;

  @ApiModelProperty(notes = "Инициатор заведения")
  @Column(name = "ACTIVATED_BY", updatable = false)
  @CreatedBy
  private String activatedBy;

  @ApiModelProperty(notes = "Дата заведения")
  @Column(name = "ACTIVATED_DATE", updatable = false)
  @CreatedDate
  private LocalDateTime activatedDate;

  @ApiModelProperty(notes = "Комментарии по заведению")
  @Column(name = "ACT_COMMENTS")
  private String actComments;

  @ApiModelProperty(notes = "Признак активности записи")
  @Column(name = "IS_ACTIVE")
  private Boolean isActive;

  @ApiModelProperty(notes = "Инициатор деактивации")
  @Column(name = "DEACTIVATED_BY")
  @LastModifiedBy
  private String deactivatedBy;

  @ApiModelProperty(notes = "Дата деактивации")
  @Column(name = "DEACTIVATED_DATE")
  @LastModifiedDate
  private LocalDateTime deactivatedDate;

  @ApiModelProperty(notes = "Комментарии по деактивации")
  @Column(name = "DEACTIVATED_COMMENTS")
  private String deactivatedComments;

  @ApiModelProperty(notes = "Дата изменения")
  @Column(name = "MODIFIED_DATE")
  @LastModifiedDate
  private LocalDateTime modifiedDate;

  @ApiModelProperty(notes = "Кем изменено", required = true)
  @Column(name = "MODIFIED_BY", length = 50, nullable = false)
  @NotNull
  @LastModifiedBy
  private String modifiedBy;
}
