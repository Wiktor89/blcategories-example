package blacklist.categories.security;

import blacklist.categories.exception.ExceptionResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Slf4j
@RequiredArgsConstructor
public class AuthenticationExceptionHandler extends BasicAuthenticationEntryPoint {

  private final ObjectMapper mapper;

  @Override
  public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException {
    log.error("AuthenticationExceptionHandler->commence (exception in auth) correlationId={}", e);
    final HttpStatus status = HttpStatus.UNAUTHORIZED;
    final ExceptionResponse exceptionResponse = ExceptionResponse.of(status, e, httpServletRequest);
    final String responseMsg = mapper.writeValueAsString(exceptionResponse);
    httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    httpServletResponse.setContentType("application/json");
    httpServletResponse.setCharacterEncoding("UTF-8");
    httpServletResponse.addHeader("WWW-Authenticate", "Basic realm=\"" + getRealmName() + "\"");
    httpServletResponse.addHeader("Access-Control-Allow-Origin", "*");
    httpServletResponse.addHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
    httpServletResponse.addHeader("Access-Control-Max-Age", "3600");
    httpServletResponse.addHeader("Access-Control-Allow-Headers", "x-requested-with, authorization, credentials");
    httpServletResponse.getWriter().write(responseMsg);
  }

  @Override
  public void afterPropertiesSet() throws Exception {
    setRealmName("dictionary");
    super.afterPropertiesSet();
  }
}
