package blacklist.categories.security;

import blacklist.categories.exception.BadRequestException;
import blacklist.categories.utils.RequestUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class DetailsService implements UserDetailsService {

  @Override
  public UserDetails loadUserByUsername(String username) {
    log.debug("UserDetailsService -> loadUserByUsername for username= {} ", username);
    final Optional<User> credentialsHolder = RequestUtil.getFromBasicAuth();
    if (!credentialsHolder.isPresent()) {
      log.error("Can't get credentials from basic auth");
      throw new BadRequestException("Can't get credentials from basic auth");
    }
    return credentialsHolder.get();
  }
}