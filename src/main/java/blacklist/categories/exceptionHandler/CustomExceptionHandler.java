package blacklist.categories.exceptionHandler;

import blacklist.categories.exception.BadRequestException;
import blacklist.categories.exception.ExceptionResponse;
import blacklist.categories.exception.InternalServerException;
import blacklist.categories.exception.SecurityAccessException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
@Slf4j
public class CustomExceptionHandler {

  private final String LOG_MESSAGE = "RestControllerAdvice -> handleAccessDeniedException, correlationId = {} ";
  private final String ERROR_MESSAGE = "Ошибка: ";

  @ExceptionHandler(SecurityAccessException.class)
  public final ResponseEntity<ExceptionResponse> handleAccessDeniedException(SecurityAccessException ex, HttpServletRequest request) {
    log.error(LOG_MESSAGE, ex);
    final HttpStatus status = HttpStatus.FORBIDDEN;
    final ExceptionResponse exceptionResponse = ExceptionResponse.of(status.toString(), SecurityAccessException.class.getSimpleName(), "Доступ запрещен", ex.toString(), request.getRequestURI());
    return new ResponseEntity(exceptionResponse, status);
  }

  @ExceptionHandler(InternalServerException.class)
  public final ResponseEntity<ExceptionResponse> handleInternalErrorException(InternalServerException ex, HttpServletRequest request) {
    log.error(LOG_MESSAGE, ex);
    final HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
    final ExceptionResponse exceptionResponse =
        ExceptionResponse.of(status.toString(), InternalServerException.class.getSimpleName(), ERROR_MESSAGE + ex.toString(), ex.toString(), request.getRequestURI());
    return new ResponseEntity(exceptionResponse, status);
  }

  @ExceptionHandler(BadRequestException.class)
  public final ResponseEntity<ExceptionResponse> handleBadRequestException(BadRequestException ex, HttpServletRequest request) {
    log.error(LOG_MESSAGE, ex);
    final HttpStatus status = HttpStatus.BAD_REQUEST;
    final ExceptionResponse exceptionResponse = ExceptionResponse.of(status.toString(), BadRequestException.class.getSimpleName(), ERROR_MESSAGE + ex.toString(), ex.toString(), request.getRequestURI());
    return new ResponseEntity(exceptionResponse, status);
  }
}
