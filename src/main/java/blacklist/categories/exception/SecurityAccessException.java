package blacklist.categories.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class SecurityAccessException extends RuntimeException {
  public SecurityAccessException(String message) {
    super(message);
  }
}
